import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyBzeRosUO0lvss0g7SlJ2IdyzrvEs5_kzs",
  authDomain: "kioscobarbieri.firebaseapp.com",
  projectId: "kioscobarbieri",
  storageBucket: "kioscobarbieri.appspot.com",
  messagingSenderId: "428676932419",
  appId: "1:428676932419:web:8812cc4ac3dec9dda605f6"
};

const app = initializeApp(firebaseConfig);
export const db = getFirestore(app);