import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import NavBar from './components/NavBar/index';
import ItemListContainer from './containers/ItemListContainer/index';
import ItemDetailContainer from './containers/ItemDetailContainer/index';
import Cart from './components/Cart/index';
import Error from './components/Error/index';
import CustomProvider from './CartContext';

const App = () => {

  return (
    <BrowserRouter>
    <CustomProvider>
      <NavBar />
      <Routes>
        <Route path="/" element={<ItemListContainer greeting={'Tienda informática del grupo Barbieri'} />} />
        <Route path="/categories/:name" element={<ItemListContainer greeting={'Tienda informática del grupo Barbieri'} />} />
        <Route path="/itemdetail/:id" element={<ItemDetailContainer />} />
        <Route path="/cart" element={<Cart />} />
        <Route path="*" element={<Error />} />
      </Routes>
    </CustomProvider>
    </BrowserRouter>
  );
};

export default App;
