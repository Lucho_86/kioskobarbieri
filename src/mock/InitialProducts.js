export const InitialProducts = [
    {
        id: 1,
        name: "Apple iPhone 12 Pro",
        price: 500000,
        description:'APPLE IPHONE 12 PRO MAX 128 GB.',
        category: "cellphones",
        stock: 20,
        url:'../img/01.jpg'
    },
    {
        id: 2,
        name: "Apple iPhone 13 Pro", 
        price: 600000,
        description:'APPLE IPHONE 13',
        category: "cellphones",
        stock: 10,
        url:'../img/02.jpg'
    },
    {
        id: 3,
        name: "PlayStation 5",
        price: 200000,
        description:'PS5 CONSOLE',
        category: "gaming",
        stock: 30,
        url:'../img/03.jpg'
    },
    {
        id: 4,
        name: "Xbox ONE X",
        price: 180000,
        description:'XBOX ONE X CONSOLE',
        category: "gaming",
        stock: 30,
        url:'../img/04.jpg'
    },
    {
        id: 5,
        name: "Nintendo Switch",
        price: 100000,
        description:'NINTENDO SWITCH CONSOLE',
        category: "gaming",
        stock: 50,
        url:'../img/05.jpg'
    },
    {
        id: 6,
        name: "PlayStation 4",
        price: 800000,
        description:'PS4 CONSOLE',
        category: "gaming",
        stock: 70,
        url:'../img/06.jpg'
    },
    {
        id: 7,
        name: "Nvidia GeForce 1660",
        price: 1200000,
        description:'NVIDIA GEFORCE GTX 1660 TI',
        category: "hardware",
        stock: 10,
        url:'../img/07.jpg'
    },
    {
        id: 8,
        name: "DDR5 RAM Corsair",
        price: 250000,
        description:'MEMORIA RAM CORSAIR',
        category: "hardware",
        stock: 20,
        url:'../img/08.jpg'
    },
    {
        id: 9,
        name: "Intel Core i7 10TH GEN",
        price: 650000,
        description:'INTEL CORE I7 10TH',
        category: "hardware",
        stock: 10,
        url:'../img/09.jpg'
    },
    {
        id: 10,
        name: "HP 15-dw1085la",
        price: 1200000,
        description:'HP 15-DW1085LA',
        category: "laptops",
        stock: 10,
        url:'../img/10.jpg'
    },
    {
        id: 11,
        name: "Xiaomi MI PRO 15",
        price: 1000000,
        description:'XIAOMI MI LAPTOP PRO 15',
        category: "laptops",
        stock: 10,
        url:'../img/11.jpg'
    },
    {
        id: 12,
        name: "Apple MacBook Air",
        price: 1850000,
        description:'APPLE MACBOOK AIR 13.3 M1 8-CORE',
        category: "laptops",
        stock: 10,
        url:'../img/12.jpg'
    },
    {
        id: 13,
        name: "Windows 11 Home",
        price: 50000,
        description:'WINDOWS 11 HOME EDITION',
        category: "software",
        stock: 50,
        url:'../img/13.jpg'
    },
    {
        id: 14,
        name: "Office 365 Home",
        price: 30000,
        description:'OFFICE 365 HOME PREMIUM',
        category: "software",
        stock: 50,
        url:'../img/14.jpg'
    },
]